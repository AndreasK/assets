# Composer Pictures

Pictures (drawings and photos) of various composers that are shared under GPL (the pictures are GPL, not the composers).

***

# German Standard Keyboard Layout for macOS

Might be helpful for people switching between macOS and any other OS or for those who connected a standard PC keyboard to their Mac computer.

Note that unfortunately macOS cannot distinguish between [Alt] and [AltGr]. There might be solutions for this, but those require a keyboard driver and not only a keyboard table.

The keyboard layout is licensed according to GPLv3, see LICENSE file.
